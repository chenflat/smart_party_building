package com.ruoyi.project.module.dist.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

import java.util.Date;

/**
 * 任务分配对象 tb_work_dist
 * 
 * @author ruoyi
 * @date 2020-06-01
 */
public class WorkDist extends BaseEntity
{
    private static final long serialVersionUID = 1L;
	  /** 主键id */
    private Long id;


    /** 标题 */
    @Excel(name = "标题")
    private String title;

    /** 等级 */
    @Excel(name = "等级")
    private String grade;

    /** 说明 */
    @Excel(name = "说明")
    private String introduce;

    /** 开始时间 */
    @Excel(name = "开始时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm")
    private String startTime;

    /** 截至时间 */
    @Excel(name = "截至时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm")
    private String endTime;

    /** 操作人id */
    @Excel(name = "操作人id")
    private Long userId;

    /** 操作人姓名 */
    @Excel(name = "操作人姓名")
    private String userName;

    /** 操作时间 */
    @Excel(name = "操作时间", width = 30, dateFormat = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date userTime;
    /** 分配给id */
    private Long distId;

    /** 分配给 */
    @Excel(name = "分配给")
    private String distName;

    /** 任务id */
    @Excel(name = "任务id")
    private Long workId;

    /** 分值 */
    @Excel(name = "分值")
    private String score;

    /** 文件 */
    @Excel(name = "文件")
    private String workFile;

    /** 状态(1 待执行 2 执行中 3 待验证 4 退回 5 已完成) */
    @Excel(name = "状态(1 待执行 2 执行中 3 待验证 4 退回 5 已完成)")
    private String status;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setDistId(Long distId) 
    {
        this.distId = distId;
    }

    public Long getDistId() 
    {
        return distId;
    }
    public void setDistName(String distName) 
    {
        this.distName = distName;
    }

    public String getDistName() 
    {
        return distName;
    }
    public void setWorkId(Long workId) 
    {
        this.workId = workId;
    }

    public Long getWorkId() 
    {
        return workId;
    }
    public void setScore(String score) 
    {
        this.score = score;
    }

    public String getScore() 
    {
        return score;
    }
    public void setWorkFile(String workFile) 
    {
        this.workFile = workFile;
    }

    public String getWorkFile() 
    {
        return workFile;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getIntroduce() {
        return introduce;
    }

    public void setIntroduce(String introduce) {
        this.introduce = introduce;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Date getUserTime() {
        return userTime;
    }

    public void setUserTime(Date userTime) {
        this.userTime = userTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("distId", getDistId())
            .append("distName", getDistName())
            .append("workId", getWorkId())
            .append("score", getScore())
            .append("workFile", getWorkFile())
            .append("status", getStatus())
            .toString();
    }
}
