package com.ruoyi.project.module.plan.domain;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 学习计划对象 tb_plan
 * 
 * @author ruoyi
 * @date 2020-05-09
 */
public class Plan extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    private Long planId;

    /** 部门id */
    @Excel(name = "部门id")
    private Long deptId;
    private String deptName;

    /** 提交人id */
    @Excel(name = "提交人id")
    private Long userId;

    /** 提交人姓名 */
    @Excel(name = "提交人姓名")
    private String userName;

    /** 提交时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date planTime;

    /** 学习计划标题 */
    @Excel(name = "学习计划标题")
    private String planTitle;

    /** 学习计划内容 */
    @Excel(name = "学习计划内容")
    private String planContent;

    /** 图片 */
    @Excel(name = "图片")
    private String planImg;

    /** 视频 */
    @Excel(name = "视频")
    private String planVideo;

    /** 状态（0正常 1关闭） */
    @Excel(name = "状态", readConverterExp = "0=正常,1=关闭")
    private String status;

    public void setPlanId(Long planId) 
    {
        this.planId = planId;
    }

    public Long getPlanId() 
    {
        return planId;
    }
    public void setDeptId(Long deptId) 
    {
        this.deptId = deptId;
    }

    public Long getDeptId() 
    {
        return deptId;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setUserName(String userName) 
    {
        this.userName = userName;
    }

    public String getUserName() 
    {
        return userName;
    }
    public void setPlanTime(Date planTime) 
    {
        this.planTime = planTime;
    }

    public Date getPlanTime() 
    {
        return planTime;
    }
    public void setPlanTitle(String planTitle) 
    {
        this.planTitle = planTitle;
    }

    public String getPlanTitle() 
    {
        return planTitle;
    }
    public void setPlanContent(String planContent) 
    {
        this.planContent = planContent;
    }

    public String getPlanContent() 
    {
        return planContent;
    }
    public void setPlanImg(String planImg) 
    {
        this.planImg = planImg;
    }

    public String getPlanImg() 
    {
        return planImg;
    }
    public void setPlanVideo(String planVideo) 
    {
        this.planVideo = planVideo;
    }

    public String getPlanVideo() 
    {
        return planVideo;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("planId", getPlanId())
            .append("deptId", getDeptId())
            .append("userId", getUserId())
            .append("userName", getUserName())
            .append("planTime", getPlanTime())
            .append("planTitle", getPlanTitle())
            .append("planContent", getPlanContent())
            .append("planImg", getPlanImg())
            .append("planVideo", getPlanVideo())
            .append("status", getStatus())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
