package com.ruoyi.project.module.story.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 故事对象 tb_story
 * 
 * @author ruoyi
 * @date 2020-05-08
 */
public class Story extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    private Long storyId;

    /** 故事标题 */
    @Excel(name = "故事标题")
    private String storyTitle;

    /** 封面图 */
    @Excel(name = "封面图")
    private String storyPic;

    /** 故事内容 */
    @Excel(name = "故事内容")
    private String storyContent;

    /** 状态（0正常 1关闭） */
    @Excel(name = "状态", readConverterExp = "0=正常,1=关闭")
    private String status;

    public void setStoryId(Long storyId) 
    {
        this.storyId = storyId;
    }

    public Long getStoryId() 
    {
        return storyId;
    }
    public void setStoryTitle(String storyTitle) 
    {
        this.storyTitle = storyTitle;
    }

    public String getStoryTitle() 
    {
        return storyTitle;
    }
    public void setStoryPic(String storyPic) 
    {
        this.storyPic = storyPic;
    }

    public String getStoryPic() 
    {
        return storyPic;
    }
    public void setStoryContent(String storyContent) 
    {
        this.storyContent = storyContent;
    }

    public String getStoryContent() 
    {
        return storyContent;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("storyId", getStoryId())
            .append("storyTitle", getStoryTitle())
            .append("storyPic", getStoryPic())
            .append("storyContent", getStoryContent())
            .append("status", getStatus())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
