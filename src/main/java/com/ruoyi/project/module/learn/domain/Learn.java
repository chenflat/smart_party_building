package com.ruoyi.project.module.learn.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 学习对象 tb_learn
 * 
 * @author ruoyi
 * @date 2020-05-21
 */
public class Learn extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    private Long id;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 用户姓名 */
    @Excel(name = "用户姓名")
    private String userName;

    private Long deptId;
    private String deptName;


    /** 学分类型 (1 学习 2 签到) */
    @Excel(name = "学分类型 (1 学习 2 签到)")
    private String type;

    /** 资讯id */
    @Excel(name = "资讯id")
    private Long newId;

    private String newTitle;

   /** 状态 (1 未学习 (未签到) 2 已学习 (已签到)) */
    @Excel(name = "状态 (1 未学习 (未签到) 2 已学习 (已签到))")
    private String status;


    /** 学分 */
    @Excel(name = "学分")
    private Long score;

    /** 签到时间 */
    @Excel(name = "签到时间")
    private String signTime;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }

    public Long getDeptId() {
        return deptId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public void setUserId(Long userId)
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setUserName(String userName) 
    {
        this.userName = userName;
    }

    public String getUserName() 
    {
        return userName;
    }
    public void setType(String type) 
    {
        this.type = type;
    }

    public String getType() 
    {
        return type;
    }
    public void setNewId(Long newId) 
    {
        this.newId = newId;
    }

    public Long getNewId() 
    {
        return newId;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setScore(Long score) 
    {
        this.score = score;
    }

    public Long getScore() 
    {
        return score;
    }
    public void setSignTime(String signTime) 
    {
        this.signTime = signTime;
    }

    public String getSignTime() 
    {
        return signTime;
    }

    public String getNewTitle() {
        return newTitle;
    }

    public void setNewTitle(String newTitle) {
        this.newTitle = newTitle;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("userName", getUserName())
            .append("type", getType())
            .append("newId", getNewId())
            .append("status", getStatus())
            .append("score", getScore())
            .append("signTime", getSignTime())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
