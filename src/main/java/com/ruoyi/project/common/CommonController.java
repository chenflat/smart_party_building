package com.ruoyi.project.common;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.utils.QiNiuUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.file.FileUploadUtils;
import com.ruoyi.common.utils.file.FileUtils;
import com.ruoyi.framework.config.RuoYiConfig;
import com.ruoyi.framework.config.ServerConfig;
import com.ruoyi.framework.web.domain.AjaxResult;

import java.util.HashMap;
import java.util.Map;

import static com.ruoyi.common.utils.file.FileUploadUtils.extractFilename;

/**
 * 通用请求处理
 * 
 * @author ruoyi
 */
@Controller
public class CommonController
{
    private static final Logger log = LoggerFactory.getLogger(CommonController.class);

    @Autowired
    private ServerConfig serverConfig;

    /**
     * 通用下载请求
     * 
     * @param fileName 文件名称
     * @param delete 是否删除
     */
    @GetMapping("common/download")
    public void fileDownload(String fileName, Boolean delete, HttpServletResponse response, HttpServletRequest request)
    {
        try
        {
            if (!FileUtils.isValidFilename(fileName))
            {
                throw new Exception(StringUtils.format("文件名称({})非法，不允许下载。 ", fileName));
            }
            String realFileName = System.currentTimeMillis() + fileName.substring(fileName.indexOf("_") + 1);
            String filePath = RuoYiConfig.getDownloadPath() + fileName;

            response.setCharacterEncoding("utf-8");
            response.setContentType("multipart/form-data");
            response.setHeader("Content-Disposition",
                    "attachment;fileName=" + FileUtils.setFileDownloadHeader(request, realFileName));
            FileUtils.writeBytes(filePath, response.getOutputStream());
            if (delete)
            {
                FileUtils.deleteFile(filePath);
            }
        }
        catch (Exception e)
        {
            log.error("下载文件失败", e);
        }
    }

    /**
     * 通用上传请求
     */
    @PostMapping("/common/upload")
    @ResponseBody
    public AjaxResult uploadFile(MultipartFile file) throws Exception
    {
        try
        {
            // 上传文件路径
           // String filePath = RuoYiConfig.getUploadPath();
            // 上传并返回新文件名称
            //String fileName = FileUploadUtils.upload(filePath, file);
            String fileUrl =  QiNiuUtils.uploadQiNiu(file);
            String fileName = extractFilename(file);
            AjaxResult ajax = AjaxResult.success();
            ajax.put("fileName", fileName);
            ajax.put("url", fileUrl);
            return ajax;
        }
        catch (Exception e)
        {
            return AjaxResult.error(e.getMessage());
        }
    }

    /**
     * 本地资源通用下载
     */
    @GetMapping("/common/download/resource")
    public void resourceDownload(String resource, HttpServletRequest request, HttpServletResponse response)
            throws Exception
    {
        // 本地资源路径
        String localPath = RuoYiConfig.getProfile();
        // 数据库资源地址
        String downloadPath = localPath + StringUtils.substringAfter(resource, Constants.RESOURCE_PREFIX);
        // 下载名称
        String downloadName = StringUtils.substringAfterLast(downloadPath, "/");
        response.setCharacterEncoding("utf-8");
        response.setContentType("multipart/form-data");
        response.setHeader("Content-Disposition",
                "attachment;fileName=" + FileUtils.setFileDownloadHeader(request, downloadName));
        FileUtils.writeBytes(downloadPath, response.getOutputStream());
    }

    /**
     * 文件上传
     */
    @PostMapping("/common/uploadImage_QiNiu")
    @ResponseBody
    public Map<String, Object> uploadImage_QiNiu(MultipartFile file) {
        Map<String,Object> res = new HashMap<String,Object>();
        Map<String,Object> data = new HashMap<String,Object>();
        try {
            if (!file.isEmpty()) {
                /********7牛**********/
                String fileUrl = QiNiuUtils.uploadQiNiu(file);
                if(StringUtils.isNotEmpty(fileUrl)){
                    data.put("src",fileUrl);
                    res.put("code",0);
                    res.put("msg","");
                    res.put("data",data);
                }else{
                    res.put("code",1);
                    res.put("msg","上传失败，请重试");
                }
                /********7牛**********/
            }
        } catch (Exception e) {
            e.printStackTrace();
            res.put("code",1);
            res.put("msg",e.getLocalizedMessage());
        }
        return res;
    }


    /**
     * 视频上传
     */
    @PostMapping("/common/uploadVideo_QiNiu")
    @ResponseBody
    public Map<String, Object> uploadVideo_QiNiu(MultipartFile file) {
        Map<String,Object> res = new HashMap<String,Object>();
        Map<String,Object> data = new HashMap<String,Object>();
        try {
            if (!file.isEmpty()) {
                /********7牛**********/
                String fileUrl = QiNiuUtils.uploadQiniuVideo(file);
                if(StringUtils.isNotEmpty(fileUrl)){
                    data.put("src",fileUrl);
                    res.put("code",0);
                    res.put("msg","");
                    res.put("data",data);
                }else{
                    res.put("code",1);
                    res.put("msg","上传失败，请重试");
                }
                /********7牛**********/
            }
        } catch (Exception e) {
            e.printStackTrace();
            res.put("code",1);
            res.put("msg",e.getLocalizedMessage());
        }
        return res;
    }
}
